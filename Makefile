VERSION=`git describe | sed 's/^v//; s/-/./g' `
NAME="publik-imio-industrialisation"

prefix = /usr

clean:
	rm -rf sdist

DIST_FILES = \
	Makefile \
	authentic \
	combo \
	hobo \
	passerelle \
	wcs

dist: clean
	-mkdir sdist
	rm -rf sdist/$(NAME)-$(VERSION)
	mkdir -p sdist/$(NAME)-$(VERSION)
	for i in $(DIST_FILES); do \
		cp -R "$$i" sdist/$(NAME)-$(VERSION); \
	done

install:
	mkdir -p $(DESTDIR)$(prefix)/lib/python3/dist-packages/authentic2/management/commands/
	-cp authentic/*.py $(DESTDIR)$(prefix)/lib/python3/dist-packages/authentic2/management/commands/
	mkdir -p $(DESTDIR)$(prefix)/lib/python3/dist-packages/combo/data/management/commands/
	cp combo/*.py $(DESTDIR)$(prefix)/lib/python3/dist-packages/combo/data/management/commands/
	mkdir -p $(DESTDIR)$(prefix)/lib/python3/dist-packages/hobo/environment/management/commands/
	cp hobo/*.py $(DESTDIR)$(prefix)/lib/python3/dist-packages/hobo/environment/management/commands/
	mkdir -p $(DESTDIR)$(prefix)/lib/python3/dist-packages/passerelle/base/management/commands/
	-cp passerelle/*.py $(DESTDIR)$(prefix)/lib/python3/dist-packages/passerelle/base/management/commands/
	mkdir -p $(DESTDIR)$(prefix)/lib/python3/dist-packages/wcs/ctl/management/commands/
	cp wcs/*.py $(DESTDIR)$(prefix)/lib/python3/dist-packages/wcs/ctl/management/commands/

dist-bzip2: dist
	-mkdir sdist
	cd sdist && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 $(NAME)-$(VERSION)

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))
