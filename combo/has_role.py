import sys

from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('role_name')

    def handle(self, role_name, *args, **options):
        if Group.objects.filter(name=role_name).exists():
            sys.exit(0)
        sys.exit(1)
