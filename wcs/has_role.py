import argparse
import sys

from quixote import get_publisher

from wcs.ctl.management.commands import TenantCommand


class Command(TenantCommand):

    def add_arguments(self, parser):
        parser.add_argument('-d', '--domain', '--vhost', required=True, metavar='DOMAIN')
        parser.add_argument('args', nargs=argparse.REMAINDER)

    def handle(self, *args, **options):
        domain = options.pop('domain')
        self.init_tenant_publisher(domain)

        for role in get_publisher().role_class.select():
            if role.name == args[0]:
                sys.exit(0)
        sys.exit(1)
